# README #

## Welcome To ViLoSen
------------------------------------------------------

### What is this repository for? ###
--------------------------------------------------
This project is an implementation of the model, ViLoSen: Visually Impaired Location Sensor, proposed by
> * **Dr. K.S.Kuppusamy** | kskuppu@gmail.com, 
Department of Computer Science,
School of Engineering and Technology, Pondicherry University, Pondicherry
and 

This paper presents a model for sensing the location and providing customized audio instructions 
regarding the current location and nearby to the visually impaired. The model is termed as 
** “ViLoSen: Visually impaired Location Sensor”**. The model is being implemented in this project in the form of an Android application for use inside the Pondicherry University Campus at Kalapet, Pondicherry.

### Project Status ###
The app has been developed to use Spatialite, which is a spatial database extension for Android's native SQLite Database API. Using open source mapping data from Open Street Maps, a Spatialite database is embedded with the app. This makes the app to operate offline without depending on any services like Google Maps and allows many unmapped features to be mapped on OSM as well.

The App uses Android's native Text To Speech API to read out Strings of sentences providing navigational assistant to visually impaired users. 

##### Features #####
* Tell the building / marked area in which a user is current in. (Status : Database needs more refinements)
* Announce 10 nearest locations within 100metres from the user's current location. (Status : Database needs more refinements)
* Provide voice based navigational assistance to user. (Status : Route from user's location to destination can be found. Navigational assistance is immature as it can only tell which way point to move to and not the direction. Providing direction assistance is in progress. It will be done by finding and comparing the directional bearing of a road on which the user is travelling on and the bearing of the road to which the user has to turn to.)

### Tools needed ###
* Linux Distro (Developed on Fedora 23)
* SQLite
* Spatialite extension for SQLite. (This [Link](https://www.gaia-gis.it/spatialite-1.0a/instalLinux.html "SQLite and Spatialite Details") has everything regarding SQLite).
* Spatialite Tools Extension for Spatialite. See [Link](https://www.gaia-gis.it/fossil/spatialite-tools/home "Spatialite Tools"))
* Android Spatialite Core Library Compilation Process : See [link](http://www.gaia-gis.it/gaia-sins/spatialite-android/spatialite-android-build-steps.txt "Android Spatialite Library Compilation Process")
* QGIS / and other similar GIS software (__Optional__)
* Spatialite GUI (__Optional__). All the tools required for OSM Data Download and Spatialite is packaged in the Ubuntu based linux distro [__OSGeo__](http://live.osgeo.org/en/download.html "OSGeo Download Link").
* Android Studio and SDK
### How to set up? ###
------------------------------------------
##### STEP 1 : Obtaining Map Data #####
* Densely map the target area on Open Street Map(OSM). See [OSM Beginners Guide](http://wiki.openstreetmap.org/wiki/Beginners'_guide "OSM Beginners Guide")
* Download mapped data of target area from OSM as __.osm__ file. See [OSM Downloading Data](http://wiki.openstreetmap.org/wiki/Downloading_data "OSM Downloading Data")
##### STEP 2 : Setting Up System #####
* Android Studio can be installed on OSGeo or OSGeo can be used as a Virtual Machine on a host platform with Android SDK installed in it.
* The Android application is already packaged with the required library files for using Spalialite.
* Spatialite Development tutorials for Android can be found [here](https://www.gaia-gis.it/fossil/libspatialite/wiki?name=spatialite-android-tutorial "Spatialite Android Tutorials'). [Spatialite Function Reference List](https://www.gaia-gis.it/spatialite-2.3.0/spatialite-sql-2.3.0.html "Spatialite Function Reference List").
##### STEP 3 : Map Download and Setup Script #####
* The MAP/MAP_SETUP.sh script contains 4 variables for defining the rectangular boundary of the area for which the OSM map is to be  downloaded. On executing the script will download the map data through OSM API and execute the required **spatialite-tools** commands to create the spatialite file. 
* The .sqlite file will be created in the MAP/dw folder with a naming scheme pu_map_<timestamp>.sqlite. The latest or the required .sqlite file has to be put in the assets folder of the app.
--------------------------
### Contribution guidelines ###

* To Be Added 

### The People ###

* Dr. K.S.Kuppusamy : kskuppu@gmail.com
* Shaswata Dutta(reedknight) : dutta.shaswata@gmail.com