package pu.dept_cs.reedknight.vilosen2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

import pu.dept_cs.reedknight.components.AlternateColorListAdapter;
import pu.dept_cs.reedknight.models.Coordinates;

public class LocationList extends Activity {

    private final Coordinates LOCATION_LIST[] = {
            new Coordinates("Ananda Rangapillai Library", 79.85604, 12.01990),
            new Coordinates("Kalidas Boys Hostel", 79.84957, 12.03018),
            new Coordinates("Computer Science Department", 79.85466, 12.01531),
            new Coordinates("Amudham Mess For Boys", 79.85081, 12.02920),
            new Coordinates("Indian Bank and Post Office", 79.85637, 12.02171),
            new Coordinates("J. N. Auditorium", 79.85695, 12.02190),
            new Coordinates("Mother Teresa Ladies Mess", 79.84798, 12.02219),
            new Coordinates("C. V. Raman Boys Hostel", 79.85302, 12.02700),
            new Coordinates("Tagore Hostel Bus Stand", 79.85092, 12.02964),
            new Coordinates("Shopping Complex Bus Stand", 79.85361, 12.01755)
    };

    private AlternateColorListAdapter<Coordinates> list_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_list);

        // Create list view from hardcoded data for prototyping
        ListView location_list_view = (ListView) findViewById(R.id.location_list_view);

        EditText location_name = (EditText) findViewById(R.id.location_name);

        this.list_adapter = new AlternateColorListAdapter<Coordinates>(this,
                R.layout.location_list, LOCATION_LIST);

        location_list_view.setAdapter(list_adapter);

        location_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("DESTINATION", LOCATION_LIST[position].asWKT());
                Bundle extras = getIntent().getExtras();
                if (extras != null) {
                    intent.putExtra("SOURCE", new Coordinates(extras.getDouble("LNG"), extras.getDouble("LAT")).asWKT());
                }

                setResult(RESULT_OK, intent);
                finish();
            }
        });

        location_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                LocationList.this.list_adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
