package pu.dept_cs.reedknight.vilosen2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import pu.dept_cs.reedknight.models.Coordinates;
import pu.dept_cs.reedknight.models.DeviceDebugLogger;
import pu.dept_cs.reedknight.models.GPSListener;
import pu.dept_cs.reedknight.models.LocationUpdateListener;
import pu.dept_cs.reedknight.models.Navigation;
import pu.dept_cs.reedknight.models.SpDBHelper;
import pu.dept_cs.reedknight.models.TTSHelper;

/**
 * <h2>MainActivity</h2>
 * <p>
 *     This is the app's entry point Activity. It handles the main controller for the
 *     app.
 * </p>
 *
 */
public class MainActivity extends Activity {

    private static final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 1;
    private final String CUR_LNG = "LNG";
    private final String CUR_LAT = "LAT";

    private final byte REQUEST_CODE = 12; // L-12. [L]ocation

    private Context appContext;

    private Button btnPrimary;
    private Button btnNearby;
    private Button btnNavigate;

    private SpDBHelper spatialiteDb;
    private TTSHelper tts;
    private GPSListener gps;
    private Navigation navigator;

    private Toast toast;

    private Coordinates destination;
    private Coordinates source;

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                    if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Nothing to do here
                } else {
                    // Exit App
                    Toast.makeText(MainActivity.this, "Fine Location Access Denied", Toast.LENGTH_SHORT)
                            .show();
                    System.exit(1);
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        gps.startGPSListener();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new DeviceDebugLogger(this).log("APP", "APP ON_CREATE METHOD FIRED");

        appContext = getApplicationContext();

        spatialiteDb = new SpDBHelper(appContext);

        tts = new TTSHelper(appContext);

        toast = Toast.makeText(this, "", Toast.LENGTH_SHORT);

        gps = new GPSListener(this, toast, tts);

        navigator = new Navigation(tts, spatialiteDb);

        btnPrimary = (Button) findViewById(R.id.btnCurrentLocation);
        btnNearby = (Button) findViewById(R.id.btnNearbyLocations);
        btnNavigate = (Button) findViewById(R.id.btnNavigate);

        btnPrimary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!gps.isGPSEnabled()) {
                    return;
                }
                if (tts.isSpeaking()) {
                    tts.stop();
                } else {
                    findCurrentLocation(gps.getLocation());
                }
            }
        });

        btnNearby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Location location = gps.getLocation();
                if (!gps.isGPSEnabled()) {
                    return;
                }
                if (tts.isSpeaking()) {
                    tts.stop();
                } else {
                    findNearbyLocations(location);
                }
                return;
            }
        });

        btnNavigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!gps.isGPSEnabled() || gps.getLocation() == null) {
                    tts.stop();
                    tts.speak("Unable to triangulate G P S location.");
                    return;
                }

                if(!navigator.isNavigating() ) {
                    Intent intent = new Intent(getBaseContext(), LocationList.class);
                    intent.putExtra(CUR_LNG, gps.getLocation().getLongitude());
                    intent.putExtra(CUR_LAT, gps.getLocation().getLatitude());
                    startActivityForResult(intent, REQUEST_CODE);
                } else {
                    stopNavigation();
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
            tts.stop();
        toast.cancel();
        gps.stopGPSListener();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Close the database connection
        try {
            toast.cancel();
            tts.close();
            spatialiteDb.close();
            gps.stopGPSListener();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startNavigation() {
        tts.stop();
        tts.speak("Route Found. Commencing Navigation");
        navigator.startNavigating();
        gps.setLocationUpdateListener(new LocationUpdateListener() {
            @Override
            public void onLocationChanged(Location location) {
                locationChanged(location);
            }
        });
    }

    /**
     * This function is called when a location change is detected during navigation. It is called
     * to trigger a sequence of actions to be taken to provide navigational assistance to the user.
     */
    private void locationChanged(Location location) {
        Coordinates currentLocation = new Coordinates(location.getLongitude(), location.getLatitude());
        byte status = -1;
        // CHECK IF USER IS ON THE ROUTE
        if(!navigator.isPointOnRoute(currentLocation) ||
                (status = navigator.navigate(currentLocation)) == Navigation.NOT_ON_ROUTE) {
            navigator.stopNavigation();
            tts.speak("You have diverted from route. Rerouting");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(destination != null && !navigator.findRoute(currentLocation, destination))
                return;
            else
                tts.speak("Route Found. Commencing Navigation");
        }

        // USER IS ON THE ROUTE. GIVE APPROPRIATE INSTRUCTIONS
        if(status == Navigation.DESTINATION_REACHED) {
            tts.speak("You have reached your destination.");
            stopNavigation();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE) {
            if (data.hasExtra("DESTINATION") && data.hasExtra("SOURCE")) {
                destination = new Coordinates(data.getExtras().getString("DESTINATION"));
                source = new Coordinates(data.getExtras().getString("SOURCE"));
                Log.d("DEG", source.toString());
                if(destination != null && navigator.findRoute(source, destination)) {
                    startNavigation();
                }
            }
        }
    }

    /**
     * This function is used to call functions in the model objects to
     * stop an ongoing navigation by de-referencing the GPS LocationUpdateListener
     */
    private void stopNavigation() {
        navigator.stopNavigation();
        gps.unsetLocationUpdateListener();
        tts.speak("Navigation Stopped");
    }

    /**
     * If user is in a building / mapped boundary, then announce the location to the user.
     * @param location :
     */
    private void findCurrentLocation(Location location) {
        String tts_out;
        if (spatialiteDb.isAvailable() && location != null) {
            double lat = location.getLatitude();
            double lng = location.getLongitude();
            tts_out = spatialiteDb.findLocation(lat, lng);
//                        Log.d("DEBUG", "HELLo q : " + tts_out);
//                        Log.d("DEBUG", "Lat : " + location.getLatitude() + "\nLon : " + location.getLongitude());
            if (tts.canSpeak()) {
                if (tts_out != null && !tts_out.isEmpty())
                    tts.speak("Your location is at: " + tts_out);
                else
                    tts.speak("Sorry! Your location could not be found.");
            }
        } else {
            if (tts.canSpeak())
                tts.speak("Unable to triangulate G P S location.");
        }
    }

    /**
     * Call models to announce the locations within 100metres of the user's current location.
     * @param location
     */
    private void findNearbyLocations(Location location) {
        String tts_out;
        if(location == null) {
            if(tts.canSpeak()) {
                tts.speak("Unable to triangulate G P S location.");
            }
            return;
        }
        double lat = location.getLatitude();
        double lng = location.getLongitude();
        tts_out = spatialiteDb.findNearbyLocations(lat, lng);
//                    Log.d("DEBUG", tts_out);
        if (tts.canSpeak()) {
            if (tts_out != null && !tts_out.isEmpty())
                tts.speak("Your nearby locations are: " + tts_out);
            else
                tts.speak("Sorry! No nearby locations could be found");
        }
    }
}