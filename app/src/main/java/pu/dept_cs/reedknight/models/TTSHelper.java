package pu.dept_cs.reedknight.models;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.widget.Toast;

import java.util.Locale;

/**
 * Model to handle the TTS operations
 * @author reedknight
 * @since 2/18/16.
 */
public class TTSHelper implements TextToSpeech.OnInitListener {
    private TextToSpeech tts;

    private Context context;

    private boolean canSpeak = false;

    public TTSHelper(Context context) {
        this.context = context;
        tts = new TextToSpeech(context, this);
    }

    /* Init function for TTS API */
    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            int result = tts.setLanguage(Locale.UK);
            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("ERROR TTS", "UK ENGLISH Language is not supported.");
                Toast.makeText(context, "UK ENGLISH Language is not supported.", Toast.LENGTH_SHORT).show();
                canSpeak = false;
            } else {
                tts.setSpeechRate((float) 0.9);
                canSpeak = true;toString();
                tts.speak("Welcome To ViLoSen", TextToSpeech.QUEUE_FLUSH, null);
            }
        }
        else {
            canSpeak = false;
            Log.e("ERROR TTS", "Failed to initialize TTS engine.");
            Toast.makeText(context, "Failed to initialize TTS engine.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Annouces a message passed to it
     * @param text Message to be announced
     */
    public void speak(String text) {
        if(tts.isSpeaking()) tts.stop();

        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        // Ignore deprecation warning. Support for API 14 required. Updated implementation requires API 21
    }

    /**
     * Close TTS
     */
    public void close() {
        tts.stop();
        tts.shutdown();
    }

    /**
     * Checks if the TTS thread can annouce something
     * @return boolean
     */
    public boolean canSpeak() { return canSpeak; }

    /**
     * Checks if the TTS thread is announcing something at an instance
     * @return boolean
     */
    public boolean isSpeaking() { Log.d("TTS D", "SPEAKING");return tts.isSpeaking(); }

    /**
     * Stop any ongoing TTS announcement
     */
    public void stop() { tts.stop(); }
}
