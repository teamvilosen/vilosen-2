package pu.dept_cs.reedknight.models;

import android.util.Log;

import java.util.ArrayList;

/**
 * This class uses an ArrayList to store the coordinates of way-points that forms a route
 * @author reedknight
 * @since 3/12/16.
 */
public class Route {

    private ArrayList<Coordinates> route;
    private String routeError;
    private int currentSegment;

    Route() {
        route = new ArrayList<Coordinates>();
        currentSegment = 0;
    }

    /**
     * Adds a coordinate to the route
     * @param coords Coordinate to be added to route
     */
    public void addLocation(Coordinates coords) {
        route.add(coords);
    }

    /**
     * Adds way-point in the beginning of the route
     * @param coords Coordinate to be added to route
     */
    public void addFirstLocation(Coordinates coords) {
        route.add(0, coords);
    }

    /**
     * Parses a WKT LineString, fetches the way-points and appends to the route
     * @param routeAsWKT Linestring as WKT to be appended to route
     */
    public void appendFromWKT(String routeAsWKT) {
        if(routeAsWKT == null || routeAsWKT.isEmpty())
            return;
        int finish = routeAsWKT.length();
        int start_index = 11;
        do {
            int end_index = routeAsWKT.indexOf(',', start_index);
            if(end_index == -1)
                end_index = routeAsWKT.indexOf(')',start_index);
            String crds = routeAsWKT.substring(start_index, end_index);
            double lng = Double.parseDouble(crds.substring(0, crds.indexOf(' ')));
            double lat = Double.parseDouble(crds.substring(crds.indexOf(' ') + 1));

            Coordinates coords = new Coordinates(lng, lat);

            route.add(coords);
            start_index = end_index + 2;
        } while(start_index < finish - 1);
    }

    /**
     * Set an error message with route. Route may not exist or partially formed because of some
     * reasons.
     * @param message Message
     */
    public void setErrorMessage(String message) {
        this.routeError = message;
    }

    /**
     * As the user navigates on the route, she crosses from on segment to another. Returns the
     * current segment of the route the user is on.
     * @return current segment number
     */
    public int getCurrentSegment() {
        return this.currentSegment;
    }

    /**
     * @unused : To increment the movement of user to the next segment from the current segment
     */
    public boolean nextSegment() {
        if(this.currentSegment < route.size() - 2) {
            Log.d("DEBUG RT", "NEXT");
            this.currentSegment++;
            return true;
        }
        return false;
    }

    /**
     * Return the route as a WKT LineString.
     * @return Route as WKT
     */
    public String toWKT() {
        StringBuilder linestring = new StringBuilder("LINESTRING(");
        for(int i = 0; i < route.size(); i++) {
            linestring.append(route.get(i).getLongitude());
            linestring.append(' ');
            linestring.append(route.get(i).getLatitude());
            if(i + 1 != route.size())
                linestring.append(", ");
        }
        linestring.append(')');
        return String.valueOf(linestring);
    }

    public void replaceLast(Coordinates coords) {
        route.remove(route.size() - 1);
        route.add(coords);
    }

    public void replaceFirst(Coordinates coords) {
        route.remove(0);
        route.add(0, coords);
    }

    public String getEndSegmentAsWKT() {
        return getSegmentAsWKT(route.size()-2);
    }

    public String getBeginSegmentAsWKT() {
        return getSegmentAsWKT(0);
    }

    /**
     * Get a segment as a WKT Linestring of 2 coordinates
     * @param index index of waypoint from where the segment starts
     * @return WKT Linestring of the segment
     */
    public String getSegmentAsWKT(int index) {
        if(route.size() > 1 && route.size()-2 >= index) {
            StringBuilder linestring = new StringBuilder("LINESTRING(");
            linestring.append(route.get(index).getLongitude());
            linestring.append(' ');
            linestring.append(route.get(index).getLatitude());
            linestring.append(", ");
            linestring.append(route.get(index+1).getLongitude());
            linestring.append(' ');
            linestring.append(route.get(index+1).getLatitude());
            linestring.append(')');
            return String.valueOf(linestring);
        }
        return null;
    }

    public String getErrorMessage() {
        return this.routeError;
    }

    public int size() {
        return route.size();
    }

    /**
     * Return coordinates of n-th way-point on route.
     * @param index index of the way-point
     * @return Coordinates of the way-point
     */
    public Coordinates getWayPoint(int index) {
        return route.get(index);
    }

    /**
     * Check if the route is empty.
     * @return boolean
     */
    public boolean isEmpty() {
        return (route == null || route.isEmpty() || route.size() < 1);
    }

    /**
     * Set a segment to be current segment.
     * @param currentSegment segment index
     */
    public void setCurrentSegment(int currentSegment) {
        this.currentSegment = currentSegment;
    }
}
