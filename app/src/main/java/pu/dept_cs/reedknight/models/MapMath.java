package pu.dept_cs.reedknight.models;

/**
 * Contains Geo-Utility functions.
 * @author reedknight
 * @since 3/31/16.
 */
public class MapMath {

    /* Returned as corresponding directions for directionChangeFromBearings() method */
    public final static byte LEFT = -1;
    public final static byte STRAIGHT = 0;
    public final static byte RIGHT = 1;


    /**
     * Takes two coordinates and returns the bearing w.r.t true north in degrees.
     * @param c1 Coordinate of point 1
     * @param c2 Coordinate of point 2
     * @return bearing in degrees
     * @link http://www.movable-type.co.uk/scripts/latlong.html
     * @link http://stackoverflow.com/a/9462757
     */
    public static double getBearing(Coordinates c1, Coordinates c2) {
        double longitude1 = c1.getLongitude();
        double longitude2 = c2.getLongitude();
        double latitude1 = Math.toRadians(c1.getLatitude());
        double latitude2 = Math.toRadians(c2.getLatitude());
        double longDiff= Math.toRadians(longitude2-longitude1);
        double y= Math.sin(longDiff)*Math.cos(latitude2);
        double x=Math.cos(latitude1)*Math.sin(latitude2)-Math.sin(latitude1)*Math.cos(latitude2)*Math.cos(longDiff);

        return (Math.toDegrees(Math.atan2(y, x))+360)%360;
    }

    /**
     * Takes two coordinates and returns the distance between them using the Haversine formula
     * @param c1 Coordinate of point 1
     * @param c2 Coordinate of point 2
     * @return distance in metres (Rounded to whole number)
     * @link http://www.movable-type.co.uk/scripts/latlong.html
     */
    public static double distance(Coordinates c1, Coordinates c2) {
        double d2r = Math.PI / 180;
        double dLong = (c1.getLongitude() - c2.getLongitude()) * d2r;
        double dLat = (c1.getLatitude() - c2.getLatitude()) * d2r;
        double a = Math.pow(Math.sin(dLat / 2.0), 2) + Math.cos(c2.getLatitude() * d2r)
                * Math.cos(c1.getLatitude() * d2r) * Math.pow(Math.sin(dLong / 2.0), 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = 6367000 * c;
        return Math.round(d);
    }

    /**
     * Takes two cardinal bearings; bearing of "from road" and bearing of and "to road" and
     * returns left / right turn. If bearing difference is < 20deg, then return straight.
     * Turn should not be some absurd value like 180deg since no one will need to turn 180deg!
     * @author : reedknight
     * @param q1 Bearing of "From Road" in degrees
     * @param q2 Bearing of "Destination Road" in degrees
     * @return Constant LEFT/RIGHT/STRAIGHT
     */
    public static byte directionChangeFromBearings(double q1, double q2) {
        double diff = q1 - q2;
        byte direction;

        if(Math.abs(diff) < 20)
            direction = STRAIGHT;
        else if(diff < 0) {
            if(Math.abs(diff) > 180)
                direction = LEFT;
            else
                direction = RIGHT;
        } else {
            if(diff > 180)
                direction = RIGHT;
            else
                direction = LEFT;
        }

        return direction;
    }

}
