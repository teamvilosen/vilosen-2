package pu.dept_cs.reedknight.models;

import android.location.Location;
import android.util.Log;

import java.util.Map;

/**
 * Model to provide TTS based navigation functionality.
 * @author reedknight
 * @since 3/13/16.
 */
public class Navigation {

    private Route route;
    private TTSHelper tts;
    private SpDBHelper spatialiteDb;

    private boolean isNavigating = false;

    public static final byte NOT_ON_ROUTE = 0;
    public static final byte DESTINATION_REACHED = 1;
    public static final byte ON_ROUTE = 2;

    public Navigation(TTSHelper tts, SpDBHelper spatialiteDb) {
        this.tts = tts;
        this.spatialiteDb = spatialiteDb;
    }

    /**
     * Checks it user is navigating or not. Set to false after user reaches destination
     * @return boolean
     */
    public boolean isNavigating() {
        return isNavigating;
    }

    /**
     * Set the navigation status to false. Called if user diverts from route, stops navigation or reaches intended
     * destination
     */
    public void stopNavigation() {
        isNavigating = false;
    }

    /**
     * Set the navigation status to true.
     */
    public void startNavigating() {
        isNavigating = true;
    }

    /**
     * Finds on which line-segment of the route the given coordinate is on.
     * @param c Coordinate to be operated on
     * @return
     */
    private int findSegmentOn(Coordinates c) {
        for(int i = route.getCurrentSegment(); i < route.size() - 1; i++) {
            if(spatialiteDb.onLineString(route.getSegmentAsWKT(i), c.asWKT())) {
                route.setCurrentSegment(i);
                return i;
            }
        }
        return -1;
    }

    /**
     * Announce navigational assistance based on the Coordinate provided. It is supposed to be the
     * user's current location.
     * @param current Current coordinates of the user
     * @return
     */
    public byte navigate(Coordinates current) {
        int segment = this.findSegmentOn(current);
        String turn = "";

        if(segment == -1) {
            return NOT_ON_ROUTE;
        }
        int target_waypoint = route.getCurrentSegment() + 1;
        int distance = (int) MapMath.distance(current, route.getWayPoint(target_waypoint));
        String way_point_name;
        Log.d("SEGMENT", "Size : " + route.size() + ", Seg : " + segment);

        if(segment == route.size() - 2  ) {
            way_point_name = "destination";
        } else {
            way_point_name = "next way point"; // TODO : Figure out how to fetch road name from spatialite based on segment
        }

        if(!way_point_name.equalsIgnoreCase("destination") && spatialiteDb.pointIsRoadNode(route.getWayPoint(target_waypoint).asWKT())) {
            // Check if way point is a node
            // If yes, then check which way the user has to turn.
            double bearing1 = MapMath.getBearing(
                    route.getWayPoint(target_waypoint - 1),
                    route.getWayPoint(target_waypoint)
            );

            double bearing2 = MapMath.getBearing(
                    route.getWayPoint(target_waypoint),
                    route.getWayPoint(target_waypoint + 1)
            );

            byte turn_direction = MapMath.directionChangeFromBearings(bearing1, bearing2);

            Log.d("DEBUG-TURN", bearing1 + ", " + bearing2 + ", " + turn_direction);

            if(turn_direction == MapMath.LEFT)
                turn = "Turn left";
            else if(turn_direction == MapMath.RIGHT)
                turn = "Turn right";
            else
                turn = "Go straight";

        }

        tts.stop();
        if(distance <= 5) {
            if(way_point_name.equalsIgnoreCase("destination")) {
                // tts.speak("Reach destination after " + distance + " meters.");
                return DESTINATION_REACHED;
            } else if(!turn.isEmpty()) {
                tts.speak(turn + " after " + distance + " meters.");
            } else
                tts.speak("Way Point change after " + distance + " meters.");
        } else {
            if(!turn.isEmpty())
                tts.speak(turn + " after " + distance + " meters.");
            else
                tts.speak(String.format("You are %d metres from " + way_point_name, distance));

        }
        return ON_ROUTE;
    }

    /**
     * Function calls the spatialite DB to find out the route.
     * TODO : Update UI and procedure calls. Currently destination is hardcoded
     * @param source The source coordinate
     * @param destination The destination coordinate
     * @return boolean: Route found or not
     */
    public boolean findRoute(Coordinates source, Coordinates destination) {

        if (source == null || destination == null) {
            tts.stop();
            tts.speak("Unable to triangulate G P S location.");
            return false;
        }

        // Location Found
        if (spatialiteDb.isAvailable()) {

//            Coordinates destination = new Coordinates(79.85466,12.01531); // destination selection need UI and seperate module
//            Coordinates source = new Coordinates(location.getLongitude(), location.getLatitude());

            Route raw_route = spatialiteDb.findRoute(source, destination); // destination test
            this.route = spatialiteDb.sanitizeRoute(raw_route);
            this.route.setErrorMessage(raw_route.getErrorMessage());

            if (route == null || route.isEmpty()) {
                tts.stop();
                if (!(route.getErrorMessage() == null) && !route.getErrorMessage().isEmpty())
                    tts.speak(route.getErrorMessage());
                else
                    tts.speak("Route to selected destination does not exist.");
                return false;
            }

            // Route Found
            Log.d("ROUTE", route.toWKT());

            // Start navigation
            tts.stop();
            return true;
        }
        return false;
    }

    /**
     * Checks if a point is on the linestring of the route.
     * @param coords Coordinates of the point to be checked
     * @return
     */
    public boolean isPointOnRoute(Coordinates coords) {
        return spatialiteDb.onLineString(route.toWKT(), coords.asWKT());
    }
}
