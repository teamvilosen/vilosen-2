package pu.dept_cs.reedknight.models;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * This class contains methods used for logging app data for debugging.
 * @author reedknight
 * @since 3/28/16
 */
public class DeviceDebugLogger {

    Context context = null;

    public DeviceDebugLogger(Context context) {
        this.context = context;
    }


    /**
     * Takes a key and a message to log and appends it to a file "vilosen2_debug.log" in the
     * downloads folder of the device.
     * @param key A key for the message to be logged
     * @param log The message
     */
    public void log(String key, String log) {
        try {

            String storageState = Environment.getExternalStorageState();

            if (storageState.equals(Environment.MEDIA_MOUNTED)) {

                File file = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DOCUMENTS), "vilosen2_debug.log");

                FileOutputStream fos2 = new FileOutputStream(file, true);

                DateFormat currentDateTimeString = DateFormat.getDateTimeInstance();
                currentDateTimeString.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));

                String msg = currentDateTimeString.format(new Date()) + " :: " + key + " : " + log + "\n";

                fos2.write(msg.toString().getBytes());

                fos2.close();
            }
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
}
