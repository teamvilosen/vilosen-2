package pu.dept_cs.reedknight.models;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Exception;
import java.util.Arrays;
import java.util.Map;

import jsqlite.*;
import pu.dept_cs.reedknight.vilosen2.R;

/**
 * Model to handle Spatialite Database Operations
 * @author reedknight
 * @since 2/16/16.
 */
public class SpDBHelper {

    private String DB_PATH;
    private Database db;
    private Context context;

    /**
     * heck if spatialite db exists in app data folder. If not, then copy the .sqlite file
     * from the assets folder to the app data folder.
     * @param context Application Context
     */
    public SpDBHelper(Context context) {
        super();
        this.context = context;
        DB_PATH = "/data/data/" + context.getApplicationContext().getPackageName() + "/databases/";
        try {
            File fileSqlite = new File(DB_PATH);

            if(!fileSqlite.exists()) {
                Log.v("DEBUG", "File Not Exist");
                AssetManager asstMngr = context.getResources().getAssets();
                Log.v("DEBUG", Arrays.toString(asstMngr.list(".")));
                InputStream in = asstMngr.open("pu_map.sqlite");

                fileSqlite.mkdirs();

                OutputStream out = new FileOutputStream(fileSqlite.getAbsolutePath() + "/pu-map-spatialite.sqlite", false);

                byte[] buffer = new byte[1024];
                int length;
                while ((length = in.read(buffer)) > 0) {
                    out.write(buffer, 0, length);
                }
                in.close();
                out.close();
            }

            fileSqlite = new File(DB_PATH);

            this.db = new jsqlite.Database();
            this.db.open(fileSqlite.getAbsolutePath() + "/pu-map-spatialite.sqlite", Constants.SQLITE_OPEN_READONLY);

        } catch(FileNotFoundException eF) {
            eF.printStackTrace();
            db = null;
        } catch(Exception e) {
            e.printStackTrace();
            db = null;
        }
    }

    /**
     * Close database connection
     */
    public void close() {
        try {
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Check if the spatialite database is available and has been properly initialized for operation
     * @return boolean
     */
    public boolean isAvailable() { return (db == null)?false:true; }

    /**
     * Get distance between 2 geometries.
     * @param geom1 WKT of geometry 1
     * @param geom2 WKT of geometry 2
     * @return int Distance
     */
    public int distanceBetweenGeometries(String geom1, String geom2) {
        try {
            String queryCheckTouches = String.format(context.getResources().getString(R.string.queryCheckPointTouches), geom1, geom2);
            Stmt stmt = db.prepare(queryCheckTouches);
            if(stmt.step())
                return (int)stmt.column_double(0);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
        return 0;
    }

    /**
     * Executes spatialite query to check if a coordinate is on a given linestring
     * @param linestring WKT Linestring
     * @param coords WKT Coordinates
     * @return boolean On linstring or not
     */
    public boolean onLineString(String linestring, String coords) {
        if(distanceBetweenGeometries(linestring, coords) <= 20) /* 20 meters as buffer. */
            return true;
        else
            return false;
    }

    /**
     * Execute spatialite query to find route between source and destination.
     * @param source Source Coordinate
     * @param destination Destination Coordinate
     * @return
     */
    public Route findRoute(Coordinates source, Coordinates destination) {
        Route route = new Route();
        try {
            String query;
            Stmt stmt;

            int source_road_id = 0, source_road_node_from = 0, source_road_node_to = 0, source_node = 0;
            int  destination_road_id = 0, destination_road_node_from = 0, destination_road_node_to = 0, destination_node = 0;
            double source_distance_from_road = 0, destination_distance_from_road = 0;
            String source_segment_1 = "", source_segment_2 = "", destination_segment_1 = "", destination_segment_2 = "";

            // Check if user is already at destination
            if((int)MapMath.distance(source, destination) <= 5) {
                route.setErrorMessage("You are already at the selected destination");
                return route;
            }

            // Check if source is within 5meters from a node
            query = context.getResources().getString(R.string.queryDistanceFromNearestNode);
            stmt = db.prepare(String.format(query, source.getLongitude(), source.getLatitude()));
            if(stmt.step()) {
                double distance_from_nearest_node = stmt.column_double(1);
                if(distance_from_nearest_node <= 5) {
                    source_node = stmt.column_int(0);
                }
            }

            // Check if destination is within 5meters from a node
            query = context.getResources().getString(R.string.queryDistanceFromNearestNode);
            stmt = db.prepare(String.format(query, destination.getLongitude(), destination.getLatitude()));
            if(stmt.step()) {
                double distance_from_nearest_node = stmt.column_double(1);
                if(distance_from_nearest_node <= 5) {
                   destination_node = stmt.column_int(0);
                }
            }

            if(source_node == 0) {
                // Get query details for source
                query = context.getResources().getString(R.string.queryNearestRoad);
                stmt = db.prepare(String.format(query, source.getLongitude(), source.getLatitude()));
                if (stmt.step()) {
                    source_road_id = stmt.column_int(0);
                    source_segment_1 = stmt.column_string(1);
                    source_segment_2 = stmt.column_string(2);
                    source_road_node_from = stmt.column_int(3);
                    source_road_node_to = stmt.column_int(4);
                    source_distance_from_road = stmt.column_double(5);
                    Log.d("source_road_id", String.valueOf(source_road_id));
                    Log.d("source_segment_1", String.valueOf(source_segment_1));
                    Log.d("source_segment_2", String.valueOf(source_segment_2));
                    Log.d("source_road_node_from", String.valueOf(source_road_node_from));
                    Log.d("source_road_node_to", String.valueOf(source_road_node_to));
                    Log.d("source_dist_frm_road", String.valueOf(source_distance_from_road));
                }
            } else Log.d("Source Node", String.valueOf(source_node));

            if(destination_node == 0) {
                // Get query details for destination
                query = context.getResources().getString(R.string.queryNearestRoad);
                stmt = db.prepare(String.format(query, destination.getLongitude(), destination.getLatitude()));
                if (stmt.step()) {
                    destination_road_id = stmt.column_int(0);
                    destination_segment_1 = stmt.column_string(1);
                    destination_segment_2 = stmt.column_string(2);
                    destination_road_node_from = stmt.column_int(3);
                    destination_road_node_to = stmt.column_int(4);
                    destination_distance_from_road = stmt.column_double(5);
                    Log.d("destination_road_id", String.valueOf(destination_road_id));
                    Log.d("destination_segment_1", String.valueOf(destination_segment_1));
                    Log.d("destination_segment_2", String.valueOf(destination_segment_2));
                    Log.d("dest_road_node_from", String.valueOf(destination_road_node_from));
                    Log.d("dest_road_node_to", String.valueOf(destination_road_node_to));
                    Log.d("dest_dist_frm_road", String.valueOf(destination_distance_from_road));
                }
            } else Log.d("Destination Node", String.valueOf(destination_node));

            if(source_node != 0 && destination_node != 0) {
                // Destination and source are same
                if(source_node == destination_node) {
                    route.setErrorMessage("You are already at the selected location");
                    return route;
                } else {
                    // We know exact nodes to route. So route from source node to destination node
                    String routeWKT = getNodeToNodeRoute(source_node, destination_node);
                    if(routeWKT != null && !routeWKT.isEmpty() && routeWKT.length() > 12 /*LINESTRING() contains 12 chars*/) {
                        route.appendFromWKT(routeWKT);
                    } else {
                        route.setErrorMessage("Route could not be found");
                        return route;
                    }
                }
            }

            if(source_road_id == destination_road_id) {

                // Route lies on same road segment
                Coordinates source_first = null, destination_last = null;
                query = context.getResources().getString(R.string.querySharedRoadSegment);
                stmt = db.prepare(String.format(query, source_segment_2, destination_segment_1));
                String routeWKT = "";
                if(stmt.step()) {
                    routeWKT = stmt.column_string(0);
                    if(routeWKT == null || routeWKT.isEmpty() || routeWKT.equalsIgnoreCase("null") ) {
                        query = context.getResources().getString(R.string.querySharedRoadSegmentReverse);
                        stmt = db.prepare(String.format(query, source_segment_1, destination_segment_2));
                        if(stmt.step()) {
                            routeWKT = stmt.column_string(0);
                            query = context.getResources().getString(R.string.queryLineStringLastPoint);
                            stmt = db.prepare(String.format(query, source_segment_1));
                            if(stmt.step())
                                source_first = new Coordinates(stmt.column_string(0));

                            query = context.getResources().getString(R.string.queryLineStringFirstPoint);
                            stmt = db.prepare(String.format(query, destination_segment_2));
                            if(stmt.step())
                                destination_last = new Coordinates(stmt.column_string(0));
                        }
                    } else {
                        query = context.getResources().getString(R.string.queryLineStringFirstPoint);
                        stmt = db.prepare(String.format(query, source_segment_2));
                        if(stmt.step())
                            source_first = new Coordinates(stmt.column_string(0));

                        query = context.getResources().getString(R.string.queryLineStringLastPoint);
                        stmt = db.prepare(String.format(query, destination_segment_1));
                        if(stmt.step())
                            destination_last = new Coordinates(stmt.column_string(0));
                    }
                }

                if(routeWKT != null && !routeWKT.isEmpty() && !routeWKT.equalsIgnoreCase("null")) {
                    route.appendFromWKT(routeWKT);

                    if(source_first != null) route.addFirstLocation(source_first);
                    if(destination_last != null) route.addLocation(destination_last);

                    query = context.getResources().getString(R.string.queryDistancePointLinestring);
                    stmt = db.prepare(String.format(query, source.asWKT(), route.toWKT()));
                    if(stmt.step() && stmt.column_double(0) >= 5) {
                        route.addFirstLocation(source);
                    }

                    stmt = db.prepare(String.format(query, destination.asWKT(), route.toWKT()));
                    if(stmt.step() && stmt.column_double(0) >= 5)
                        route.addLocation(destination);
                }
                return route;
            }

            // Now the game begins.
            // # 1 : Destination node present. Find best between source 1 and source 2
            // # 2 : Source node present. Find best between destination 1 and destination 2
            // # 3 : Source and Destination nodes not present. We have 4 alternative routes!

            // Approach 1
            int iteration = 1, count = 4;

            if(destination_node != 0) {
                count = 2;
                destination_road_node_from = destination_node;
            } else if(source_node != 0) {
                iteration = 2;
                count = 3;
                source_road_node_to = source_node;
            }
            Route alternative_routes[] = new Route[4];
            double min_distance = 0;
            int min_route_index = -1;
            for(; iteration <= count; iteration++) {
                boolean oneNodeRoute = false;
                try {
                    switch (iteration) {
                        case 1:
                            // Source FROM to destination FROM
                            Log.d("ROUTE", "CASE 1");
                            // TODO : Untested, Contains errors
                            if (source_road_node_from == destination_road_node_from) {
                                // Add segment 1 of both roads
                                route.appendFromWKT(reverseLinestring(source_segment_1));
                                route.appendFromWKT(destination_segment_1);
                                oneNodeRoute = true;
                            } else {
                                alternative_routes[0] = new Route();
                                alternative_routes[0].appendFromWKT(reverseLinestring(source_segment_1));
                                String routeWKT = getNodeToNodeRoute(source_road_node_from, destination_road_node_from);
                                if (routeWKT != null && !routeWKT.isEmpty() && routeWKT.length() > 12 /*LINESTRING() contains 12 chars*/) {
                                    alternative_routes[0].appendFromWKT(routeWKT);
                                    alternative_routes[0].appendFromWKT(destination_segment_1);
                                } else
                                    alternative_routes[0] = null;
                            }
                            break;
                        case 2:
                            // Source TO to destination FROM
                            Log.d("ROUTE", "CASE 2");
                            if (source_road_node_to == destination_road_node_from) {
                                // Add source_segment 2 and destination_segment 1
                                route.appendFromWKT(source_segment_2);
                                route.appendFromWKT(destination_segment_1);
                                oneNodeRoute = true;
                            } else {
                                alternative_routes[1] = new Route();
                                alternative_routes[1].appendFromWKT(source_segment_2);
                                String routeWKT = getNodeToNodeRoute(source_road_node_to, destination_road_node_from);
                                if (routeWKT != null && !routeWKT.isEmpty() && routeWKT.length() > 12 /*LINESTRING() contains 12 chars*/) {
                                    alternative_routes[1].appendFromWKT(routeWKT);
                                    alternative_routes[1].appendFromWKT(destination_segment_1);
                                } else
                                    alternative_routes[1] = null;
                            }
                            break;
                        case 3:
                            // Source TO to destination TO
                            Log.d("ROUTE", "CASE 3");
                            // TODO : Untested
                            if (source_road_node_to == destination_road_node_to) {
                                // Add source_segment 2 and destination_segment 2
                                route.appendFromWKT(source_segment_2);
                                route.appendFromWKT(reverseLinestring(destination_segment_2));
                                oneNodeRoute = true;
                            } else {
                                alternative_routes[2] = new Route();
                                alternative_routes[2].appendFromWKT(source_segment_2);
                                String routeWKT = getNodeToNodeRoute(source_road_node_to, destination_road_node_to);
                                if (routeWKT != null && !routeWKT.isEmpty() && routeWKT.length() > 12 /*LINESTRING() contains 12 chars*/) {
                                    alternative_routes[2].appendFromWKT(routeWKT);
                                    alternative_routes[2].appendFromWKT(reverseLinestring(destination_segment_2));
                                } else
                                    alternative_routes[2] = null;
                            }
                            break;
                        case 4:
                            // Source FROM to destination TO
                            Log.d("ROUTE", "CASE 4");
                            if (source_road_node_from == destination_road_node_to) {
                                // Add source_segment 2 and destination_segment 2
                                route.appendFromWKT(reverseLinestring(source_segment_1));
                                route.appendFromWKT(reverseLinestring(destination_segment_2));
                                oneNodeRoute = true;
                            } else {
                                alternative_routes[3] = new Route();
                                alternative_routes[3].appendFromWKT(reverseLinestring(source_segment_1));
                                String routeWKT = getNodeToNodeRoute(source_road_node_from, destination_road_node_to);
                                if (routeWKT != null && !routeWKT.isEmpty() && routeWKT.length() > 12 /*LINESTRING() contains 12 chars*/) {
                                    alternative_routes[3].appendFromWKT(routeWKT);
                                    alternative_routes[3].appendFromWKT(reverseLinestring(destination_segment_2));
                                } else
                                    alternative_routes[3] = null;
                            }
                            break;
                    }
                } catch(StringIndexOutOfBoundsException | NullPointerException e) {
                    // ignore
                    Log.e("ROUTE", "Out of bounds");
                    continue;
                } catch(Exception e) {
                    e.printStackTrace();
                }

                double distance = getLengthOfLinestring(alternative_routes[iteration-1].toWKT());
                Log.d("DISTANCE", String.valueOf(distance) + " meters");
                if(min_distance == 0 || distance < min_distance) {
                    min_distance = distance;
                    min_route_index = iteration-1;
                }

                if(route != null && !route.isEmpty()) {

                    query = context.getResources().getString(R.string.queryDistancePointLinestring);
                    stmt = db.prepare(String.format(query, source.asWKT(), route.toWKT()));
                    if(stmt.step() && stmt.column_double(0) >= 5) {
                        route.addFirstLocation(source);
                    }

                    stmt = db.prepare(String.format(query, destination.asWKT(), route.toWKT()));
                    if(stmt.step() && stmt.column_double(0) >= 5)
                        route.addLocation(destination);
                }
                if(oneNodeRoute) return route;
            }
            Log.d("MIN DISTANCE", String.valueOf(min_distance));
            Log.d("MIN CASE", String.valueOf(min_route_index+1));
            route = alternative_routes[min_route_index];
            if(route != null && !route.isEmpty()) {

                query = context.getResources().getString(R.string.queryDistancePointLinestring);
                stmt = db.prepare(String.format(query, source.asWKT(), route.toWKT()));
                if(stmt.step() && stmt.column_double(0) >= 5) {
                    route.addFirstLocation(source);
                }

                stmt = db.prepare(String.format(query, destination.asWKT(), route.toWKT()));
                if(stmt.step() && stmt.column_double(0) >= 5)
                    route.addLocation(destination);
            }
            new DeviceDebugLogger(context).log("Route", route.toWKT());
            return route;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return route;
    }

    /**
     * Execute query to get the length / distance of a WKT linestring
     * @param linestring WKT linestring
     * @return
     */
    private double getLengthOfLinestring(String linestring) {
        String query = context.getResources().getString(R.string.queryGLength);
        try {
            Stmt stmt = db.prepare(String.format(query, linestring));
            if(stmt.step()) return stmt.column_double(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Execute query to reverse a WKT LineString
     * @param wkt WKT Linestring
     * @return Reversed Linestring
     */
    private String reverseLinestring(String wkt) {
        String query = context.getResources().getString(R.string.queryReverseLinestring);
        try {
            Stmt stmt = db.prepare(String.format(query, wkt));
            if(stmt.step()) {
                return stmt.column_string(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Execute query to get route from one OSM Road Node to another OSM Road Node. This routing is
     * done using Dikstra's Single source shortest path algorithm in the SpatialiteDB using the
     * spatialite road network tool.
     * @param node_source source node id
     * @param node_destination destination node id
     * @return
     */
    private String getNodeToNodeRoute(int node_source, int node_destination) {
        String query = context.getResources().getString(R.string.queryGetRoute);
        try {
            Stmt stmt = db.prepare(String.format(query, node_source, node_destination));
            if(stmt.step()) {
                return stmt.column_string(4);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Execute query to find where the user is. Searches buildings and boundaries.
     * @param latitude
     * @param longitude
     * @return
     */
    public String findLocation(double latitude, double longitude) {
        try {
            StringBuffer output = new StringBuffer();
            String query = String.format(context.getResources().getString(R.string.queryCurrentLocation), longitude, latitude);
            Stmt stmt = db.prepare(query);

            int index = 0;
            if( stmt.step() ) {
                String location = stmt.column_string(0);
                output.append(location);
                return output.toString();
            }
            stmt.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Execute query to find out nearby locations from a given coordinate.
     * @param latitude
     * @param longitude
     * @return
     */
    public String findNearbyLocations(double latitude, double longitude) {
        try {
            StringBuffer output = new StringBuffer();
            String query = String.format(context.getResources().getString(R.string.queryNearbyLocations), longitude, latitude);
            // Log.d("DEBUG DB", query);
            Stmt stmt = db.prepare(query);

            int index = 0;
            String location;
            int distance;
            while( stmt.step() ) {
                location = stmt.column_string(0);
                distance = (int) stmt.column_double(1);
                output.append(++index).append(". ").append(location).append(", ").append(distance).append(" meters ; ");
            }
            stmt.close();

            Log.d("DB DEBUG", output.toString());
            return output.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Execute query to sanitize a Route using spatialite's SanitizeGeometry function
     * @param raw_route Route object to be sanitized
     * @return
     */
    public Route sanitizeRoute(Route raw_route) {
        String query = context.getResources().getString(R.string.querySanitizeLinestring);
        try {
            Stmt stmt = db.prepare(String.format(query, raw_route.toWKT()));
            if(stmt.step()) {
                Route sanitized_route = new Route();
                sanitized_route.appendFromWKT(stmt.column_string(0));
                return sanitized_route;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean pointIsRoadNode(String point) {
        String query = context.getResources().getString(R.string.queryNearestNodeDistance);
        try {
            Stmt stmt = db.prepare(String.format(query, point));
            while(stmt.step()) {
                if((int) stmt.column_double(0) <= 1)
                    return true;
                return false;
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
