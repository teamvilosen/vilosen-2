package pu.dept_cs.reedknight.models;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

/**
 * Implements LocationListener interface to obtain GPS location
 * @author reedknight
 * @since 2/26/16
 */
public class GPSListener implements LocationListener {

    private static final int LOCATION_UPDATE_FREQUENCY = 5000;
    private static final int LOCATION_UPDATE_MIN_DISTANCE = 10;

    private static final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 1;

    private Location location;
    private Toast toast;
    private Context context;
    private TTSHelper tts;

    private LocationManager locationManager;

    private LocationUpdateListener locationUpdateListener;

    public GPSListener(Context context, Toast toast, TTSHelper tts) {
        this.toast = toast;
        this.context = context;
        this.tts = tts;
        // Log.d("DEBUG", "GPS Helper constructor fired");
    }

    public void setLocationUpdateListener(LocationUpdateListener locationUpdateListener) {
        this.locationUpdateListener = locationUpdateListener;
    }

    public void unsetLocationUpdateListener() {
        this.locationUpdateListener = null;
    }

    /**
     * Check if GPS is enabled on the device or not
     * @return boolean
     */
    public boolean isGPSEnabled() {
        boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if(!gpsEnabled) {
            tts.stop();
            tts.speak("Please switch on your GPS to know your location.");
        }
        return gpsEnabled;
    }

    /**
     * Start the GPS listener to listen for location updates
     */
    public void startGPSListener() {
        location = null;
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_FINE_LOCATION);

        } else {
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_UPDATE_FREQUENCY, LOCATION_UPDATE_MIN_DISTANCE, this);
        }
    }

    /**
     * Stop the GPS listener.
     */
    public void stopGPSListener() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.removeUpdates(this);
            locationManager = null;
            location = null;
        }
    }

    public Location getLocation() {
        return this.location;
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location == null || location.getAccuracy() > 30) {
            // Log.d("DEBUG", "acc : " + location.getAccuracy());
            location = null;
            return;
        }

        // For debugging
        toast.setText("Lat : " + location.getLatitude() + "\nLon : " + location.getLongitude());
        toast.show();
        new DeviceDebugLogger(context).log("Location Update","POINT(" + location.getLongitude() + " "
                + location.getLatitude() +  ")");

        this.location = location;

        if(locationUpdateListener != null)
            locationUpdateListener.onLocationChanged(this.location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        location = null;
        this.isGPSEnabled();
    }
}
