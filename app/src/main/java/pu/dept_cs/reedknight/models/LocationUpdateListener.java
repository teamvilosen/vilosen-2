package pu.dept_cs.reedknight.models;

import android.location.Location;

/**
 * The overriden onLocationChanged method is called by GPSListener when a location update takes place.
 * @author reedknight
 * @since 3/8/16.
 */
public interface LocationUpdateListener {
    public void onLocationChanged(Location location);
}
