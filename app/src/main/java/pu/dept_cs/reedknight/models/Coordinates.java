package pu.dept_cs.reedknight.models;

import android.util.Log;

/**
 * <h1>Coordinates</h1>
 * <p>
 *     This class creates an ADT to represent a geo-spherical 2D co-ordinate.
 * </p>
 * @author: reedknight
 * @since 3/12/16
 */
public class Coordinates {
    private double latitude;
    private double longitude;
    private String name;

    public Coordinates() {
        this.latitude = 0;
        this.longitude = 0;
    }

    public Coordinates(double longitude, double latitude) {
        super();
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Coordinates(String name, double longitude, double latitude) {
        super();
        this.name = name;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    /**
     * Constructor parses a coordinate in WKT format. Example : 'POINT(79.123 12.01389)'
     * @param wkt
     */
    public Coordinates(String wkt) {
        super();
        if(wkt != null && wkt.length() > 7/*POINT() has 7 chars*/) {
            this.longitude = Double.parseDouble(wkt.substring(6, wkt.indexOf(' ')));
            this.latitude = Double.parseDouble(wkt.substring(wkt.indexOf(' '), wkt.length()-1));
        }
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getName() { return name; }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setName(String name) { this.name = name; }

    @Override
    public String toString() {
        if(name == null || name.isEmpty())
            return asWKT();
        return this.name;
    }

    /**
     * Returns co-ordinate as WKT string
     * @return coordinate in WKT string
     */
    public String asWKT() {
        return String.format("POINT(%s %s)",this.longitude, this.latitude);
    }
}
