package pu.dept_cs.reedknight.components;

import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;
import android.widget.SimpleAdapter;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import pu.dept_cs.reedknight.vilosen2.R;

/**
 * Created by reedknight on 5/7/16.
 */
public class AlternateColorListAdapter<T> extends ArrayAdapter {

    public AlternateColorListAdapter(Context context, @LayoutRes int resource, @NonNull T[] objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        TextView tv = (TextView) super.getView(position, convertView, parent);
        if(position % 2 == 0) {
            view.setBackgroundResource(R.color.colorPrimary);
            tv.setTextColor(Color.WHITE);
        } else {
            view.setBackgroundResource(R.color.colorWhite);
            tv.setTextColor(Color.BLACK);
        }
        return view;
    }
}
