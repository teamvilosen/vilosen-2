#!/bin/bash

# @Author : Shaswata Dutta
# A script to automatically download osm data of Pondicherry
# University for the ViLoSen 2 app and parse it into spatialite
# format along with road networks.

# Invocation ./MAP_SETUP.sh
#################################################

## 1. GEOGRAPHICAL RECTANGULAR BOUNDARY OF PU CAMPUS ##
#  OSM data of the region covered by this rectangle will be
#  downloaded. Change as required.

minlat=12.0131000
minlon=79.8436000

maxlat=12.0363000
maxlon=79.8633000

## 2. DOWNLOAD OSM DATA
#  The API is limited to bounding boxes of about 0.5 degree
#  by 0.5 degree and you should avoid using it if possible.
#  For larger areas you might try to use XAPI, for example:
# http://overpass.osm.rambler.ru/cgi/xapi_meta?*[bbox=11.5,48.1,11.6,48.2]
#
#  Source :  http://wiki.openstreetmap.org/wiki/Downloading_data

if [ ! -d "dw" ]; then
  mkdir dw
fi
time="$(date +%s)"
file=dw/pu_map_${time}
wget -O ${file}.osm "http://api.openstreetmap.org/api/0.6/map?bbox=$minlon,$minlat,$maxlon,$maxlat"

## 3. Convert OSM data to spatialite datamsd
spatialite_osm_map -o ${file}.osm -d ${file}.sqlite

## 4. Create Road Network
spatialite_osm_net -o ${file}.osm -d ${file}.sqlite -T roads -m -tf pu_road_template

spatialite_network -d ${file}.sqlite -T roads -g geometry -c length -t node_to -f node_from -n name -o roads_net_data

spatialite ${file}.sqlite 'CREATE VIRTUAL TABLE "roads_net" USING VirtualNetwork("roads_net_data")'

## 5. Create view of consolidated polygons and points from all tables which may be listed in search
# pg_building, pg_amenity, pg_generic, pg_leisure, pg_shop, pg_tourism,
spatialite ${file}.sqlite 'CREATE VIEW "pg_view" AS
                        Select id, name, geometry from pg_building where name IS NOT NULL union
                        Select id, name, geometry from pg_amenity where name IS NOT NULL AND sub_type != "university" union
                        Select id, name, geometry from pg_generic where name IS NOT NULL union
                        Select id, name, geometry from pg_leisure where name IS NOT NULL union
                        Select id, name, geometry from pg_shop where name IS NOT NULL union
                        Select id, name, geometry from pg_tourism where name IS NOT NULL
                                '

## That's All Folks!
